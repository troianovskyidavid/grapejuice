from typing import Optional, List, Dict, Any

from pydantic import BaseModel

from grapejuice_common.hardware_info.hardware_profile import HardwareProfile
from grapejuice_common.models.wineprefix_configuration_model import WineprefixConfigurationModel

current_version: int = 3


class SettingsModel(BaseModel):
    version: int = current_version
    hardware_profile: Optional[HardwareProfile] = None

    show_fast_flag_warning: bool = False
    release_channel: str = "master"
    disable_updates: bool = False
    try_profiling_hardware: bool = True
    default_wine_home: Optional[str] = ""
    wineprefixes: List[WineprefixConfigurationModel] = []
    unsupported_settings: Dict[str, Any] = []

    class Config:
        fields = {
            "version": "__version__",
            "hardware_profile": "__hardware_profile__"
        }

    def update_version(self) -> bool:
        did_update = self.version != current_version
        self.version = current_version

        return did_update
